package jp.narr.reader.handler;

public interface DocumentHandler {
	public String getFileMimeType();

	public String getFilePrettifyClass();

	public String getFileFormattedString(String fileString);

	public String getFileScriptFiles();
}
